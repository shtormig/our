﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceDesk
{
    /// <summary>
    /// Инцидент
    /// </summary>
    public abstract class Incident
    {
        /// <summary>
        /// Номер инцидента
        /// </summary>
        private int Number;
        /// <summary>
        /// Текст инцидента
        /// </summary>
        private string Text;
        /// <summary>
        /// Автор инцидента
        /// </summary>
        private string IdAuthor;
        /// <summary>
        /// Дата создания инцидента
        /// </summary>
        private DateTime CreateDate;
        /// <summary>
        /// Откуда поступил инцидент
        /// </summary>
        private int OrignId;
    }
}