﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceDesk
{
    public interface IResponsible
    {
        Task Task { get; set; }

        void AppointResponsible();
    }
}