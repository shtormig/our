﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceDesk
{
    public interface IExecutor
    {
        Task Task { get; set; }

        void AppointExecutor();
    }
}